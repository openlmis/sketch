This diagram shows a potential technical method for communicating a backflush stock management event between OpenELIS and OpenLMIS.

Use case:  OpenELIS has lab equipment that uses reagents (stock in inventory) to carry out tests (for medical diagnostics in patient care).  OpenELIS could communicate how many tests were run on a machine over a specific time-period, which could be communicated to OpenELIS.  OpenLMIS could use this information to determine the number of reagents were consumed, and that would feed into a resupply workflow within OpenLMIS.

Terminology:

- test: a diagnostic test used in medicine for patient care
- lab equipment: a piece of equipment used in running a test
- reagent: a chemical that's used to run a test (think of an toner cartridge in a printer)
- backflushing:  a stock-counting technique that calculates how much stock in inventory was consumed, by counting how much of something else that has been used.  (e.g. I consumed Y reagent bottles because I ran X number of tests, we count X and calculate Y).  Note this does not allow the consumption to be broken down for waste (or other).  It's a fixed calculation with waste and other factors assumed in the calc.

```plantuml
@startuml
skinparam componentStyle rectangle
'skinparam linetype ortho

frame "OpenELIS" {
    component test_counter [
        Test counter

        Sums for number of tests a Device has run over a specific date-range (e.g daily, weekly, monthly).
    ]
}

stack "FHIR R4" {
    json "FHIR Location" as location {
        "identifer": "location-123",
        "name": "Quality Lab Testing"
    }

    json "FHIR Device" as device {
        "identifier": "acme-machine-123",
        "manufacturer": "ACME Lab Machines",
        "model": "Lab Tester 5000",
        "location": "location-123"
    }

    json "FHIR MeasureReport" as measure_report {
        "Type": "summary", 
        "Subject": "Device<acme-machine-123>",
        "period": {
            "start": "2023-10-01",
            "end": "2023-10-31"
        },
        "group": [
            {
                "code": "test-code",
                "measureScore": 512
            }
        ]
    }
}

frame "OpenLMIS" {
    component openelis_mediator [
        OpenELIS Mediator

        Converts FHIR MeasureReport to StockEvents using backflushing.
        Needs to lookup from Product data which Product is used for N
        tests of Device.
    ]

    component stock_management [
        Stock management

        Manages stock in inventory.
    ]

    component reference_data [
        Reference data

        Holds facilities as FHIR Locations
    ]
}

' in fhir stack
measure_report -u-> device
device -u-> location

' in OpenLMIS
reference_data -[hidden]d-> stock_management
stock_management -[hidden]d-> openelis_mediator
openelis_mediator -u-> stock_management: StockEvent
reference_data -d-> openelis_mediator: Facility(Location)

' inter-relationships
test_counter -r-> measure_report: sum(Observations)
measure_report -r-> openelis_mediator
test_counter -r-> location
reference_data -l-> location

```

The primary carrier of information is the [__MeasureReport__][measure_report] published by OpenELIS, which contains the summarization of the tests run over a given period.  This is consumed by the __OpenELIS Mediator__ which could be a part of an interoperability layer, or a microservice in OpenLMIS.  This service is responsible for performing a backflush calculation and publishing that as a StockEvent to the Stock Management Service to inform a further resupply workflow or report.

Notes:
- OpenELIS and OpenLMIS must have a __shared__ understanding of Facilities as FHIR [__Locations__][location].
- OpenELIS and OpenLMIS must have a __shared__ understanding of the [__Device__][device] models and the __stock in inventory that's used per test__.  If there isn't a source of truth for this already, PCMT might be helpful in storing that meta-data. 
- FHIR [Observation][observation] doesn't appear to be the appropriate resource as it's an event resource - tied to a singular patient or a test run.
- FHIR [DeviceMetric][device_metric] is for describing capabilities, not quantities.
- FHIR Measure should likely be included for completeness.
- The evaluation method of a Measure is potentially useful, but maybe not needed - in IHE's mCSD for example, it's not directly used.
- FHIR [R4](http://hl7.org/fhir/R4/index.html) here is used, though how these resources are used here is compatible in [R5](http://hl7.org/fhir/R5/).

[location]: http://hl7.org/fhir/R4/location.html
[measure]: http://hl7.org/fhir/R4/measure.html
[measure_report]: http://hl7.org/fhir/R4/measurereport.html#MeasureReport
[observation]: http://hl7.org/fhir/R4/observation.html
[device]: http://hl7.org/fhir/R4/device.html
[device_metric]: http://hl7.org/fhir/R4/devicemetric.html
